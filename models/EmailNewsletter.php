<?php

class EmailNewsletter
{

    public function saveEmail($email)
    {
        if ($email) {
            $exist = Db::queryAlone('
                SELECT COUNT(*)
                FROM newsletter
                WHERE email=?
                LIMIT 1
                ', array($email));
            if ($exist) {
                return false;
            } else {
                Db::query('
                INSERT INTO newsletter (email)
                VALUE (?)
                ', array($email));
                return true;
            }
        }
    }

    public function getEmails() {
        return Db::queryAll('
        SELECT *
        FROM newsletter');
    }
}