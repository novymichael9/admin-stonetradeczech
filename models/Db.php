<?php

class Db {

    /** @var PDO */
    private static $connect;

    private static $settings = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_EMULATE_PREPARES => false,
    );


    public static function connect($host, $user, $password, $database) {
        if (!isset(self::$connect)) {
            self::$connect = @new PDO(
                "mysql:host=$host;dbname=$database",
                $user,
                $password,
                self::$settings
            );
        }
    }

    public function getLastInsertId() {
        return self::$connect->lastInsertId();
    }

    public static function queryOne($query, $parameters = array())
    {
        $back = self::$connect->prepare($query);
        $back->execute($parameters);
        return $back->fetch();
    }


    public static function queryAll($query, $parameters = array()) {
        $back = self::$connect->prepare($query);
        $back->execute($parameters);
        return $back->fetchAll();
    }


    public static function queryAlone($query, $parameters = array()) {
        $result = self::queryOne($query, $parameters);
        return $result[0];
    }

    public static function queryGetAll($query, $parameters = array()) {
        $result = self::queryAll($query, $parameters);
        return $result[0];
    }


    public static function query($query, $parameters = array()) {
        $back = self::$connect->prepare($query);
        $back->execute($parameters);
        return $back->rowCount();
    }

}