<?php

class Categories
{
    public function addCategory($name, $description, $image_uri)
    {
        Db::query('
                INSERT INTO categories (name, meta_title, meta_description, image)
                VALUES (?, ?, ?, ?)
                ', array($name, $name, $description, $image_uri));
        return true;
    }

    public function getCategoryNameById($id)
    {
        return Db::queryAlone("SELECT name FROM categories WHERE id = ?", array($id));
    }

    public function getAll()
    {
        return Db::queryAll('
        SELECT id, name
        FROM categories WHERE deleted=0 ORDER BY id');
    }

    public function deleteCategory($id)
    {
        Db::query('
            UPDATE categories SET deleted=1
            WHERE id=?', array($id));
    }

    public function getVisitorStatistics($categories_id, $not_older_than)
    {
        $num = Db::queryAlone("SELECT COUNT(css.count) summary FROM (SELECT COUNT(*) count FROM categories_statistics cs WHERE cs.categories_id = ? AND (cs.created >= DATE(NOW()) + INTERVAL -? DAY) GROUP BY cs.ip) css", array($categories_id, $not_older_than));
        if ($num) {
            return $num;
        }
        return 0;
    }
}