<?php

class LoginUser {

    public function login ($username, $password)
    {
        $user = Db::queryOne('
        SELECT users_id, admin, password
        FROM users
        WHERE username=?
        ', array($username));
        if (!$user)
        {
            $_SESSION['message'] = 'Neplatné uživatelské jméno.';
        }
        else
        {
            if (sha1($password) == $user["password"]) {
                $_SESSION['user_id'] = $user['users_id'];
                $_SESSION['user_username'] = $user['users_username'];
                $_SESSION['user_admin'] = $user['admin'];
            }
            else
            {
                $_SESSION['message'] = 'Neplatné uživatelské heslo.';
            }
        }
    }

    public function logout() {
        if (isset($_GET['odhlasit']))
        {
            session_destroy();
        }
    }
}