<?php

class Products
{
    public function addProduct($productid, $productname, $url, $productnumber, $shortproductdescription, $productdescription, $productprice, $actionproductprice, $productpricetax, $category_id, $instock = 1, $productaction = 0)
    {
        if ($_POST) {
            $exist = Db::queryAlone('
                    SELECT *
                    FROM product
                    WHERE (name=? AND deleted=0)
                    ', array($productname));
            if ($exist) {
                return false;
            } else {
                Db::query('
                INSERT INTO product (id, name, url, product_number, short_description, description, price, action_price, vat, categories_id, in_stock, action)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                ', array($productid, $productname, $url, $productnumber, $shortproductdescription, $productdescription, $productprice, $actionproductprice, $productpricetax, $category_id, $instock, $productaction));
                return true;
            }
        }
    }

    public function editProduct($productid, $productname, $url, $productnumber, $shortproductdescription, $productdescription, $productprice, $actionproductprice, $productpricetax, $category_id, $instock, $productaction = 0)
    {
        Db::query('
                UPDATE product SET id = ?, name = ?, url = ?, product_number = ?, short_description = ?, description = ?, price = ?, action_price = ?, vat = ?, categories_id = ?, in_stock = ?, action = ?
                WHERE id = ?
                ', array($productid, $productname, $url, $productnumber, $shortproductdescription, $productdescription, $productprice, $actionproductprice, $productpricetax, $category_id, $instock, $productaction, $productid));
        return true;
    }

    public function showTitleProduct()
    {
        return Db::queryAll('
        SELECT id, name
        FROM product WHERE deleted=0 ORDER BY id DESC');
    }

    public function getCheapestProductByCategoryId($categories_id)
    {
        return Db::queryOne('
        SELECT *
        FROM product WHERE categories_id = ? AND deleted=0 ORDER BY price', array($categories_id));
    }

    public function showAllProduct($id)
    {
        return Db::queryOne('
        SELECT *
        FROM product
        WHERE id=? AND deleted=0', array($id));
    }

    public function deleteProduct($id)
    {
        Db::query('
            UPDATE product SET deleted=1
            WHERE id=?', array($id));
    }

    public function getPhotos($id)
    {
        return Db::queryAll('
        SELECT *
        FROM product_images WHERE product_id = ? AND main=0', array($id));
    }

    public function getMainPhoto($id)
    {
        return Db::queryOne('
        SELECT *
        FROM product_images
        WHERE product_id=? AND main=1', array($id));
    }

    public function setMainPhoto($product_id, $product_image_id)
    {
        Db::query('UPDATE product_images SET main=0 WHERE product_id = ?', array($product_id));
        Db::query('UPDATE product_images SET main=1 WHERE product_id = ? AND id = ?', array($product_id, $product_image_id));
    }

    public function deletePhoto($product_image_id)
    {
        return Db::query("DELETE FROM product_images WHERE id=?", array($product_image_id));
    }

    public function addPhoto($id, $data_uri)
    {
        Db::query('
                INSERT INTO product_images (product_id, path)
                VALUES (?, ?)
                ', array($id, $data_uri));
        return true;
    }
}