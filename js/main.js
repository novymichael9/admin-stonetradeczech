$(document).ready(function() {
    $("input[name=vat]").on("input", function(){
        $("input[name=price-notax]").val("");
        $("input[name=price]").val("");
        $("input[name=action-price-notax]").val("");
        $("input[name=action-price]").val("");
    });
    $("input[name=price-notax]").on("input", function(){
        $("input[name=price]").val($(this).val() * (($("input[name=vat]").val()/100)+1));
    });
    $("input[name=price]").on("input", function(){
        $("input[name=price-notax]").val($(this).val() / (($("input[name=vat]").val()/100)+1));
    });
    $("input[name=action-price-notax]").on("input", function(){
        $("input[name=action-price]").val($(this).val() * (($("input[name=vat]").val()/100)+1));
    });
    $("input[name=action-price]").on("input", function(){
        $("input[name=action-price-notax]").val($(this).val() / (($("input[name=vat]").val()/100)+1));
    });


    $("input[name=sale]").on("click", function(){
        if ($(this).is(":checked")) {
            $(".action").show();
        } else {
            $(".action").hide();
            $("input[name=action-price]").val("");
            $("input[name=action-price-notax]").val("");
        }
    });
});