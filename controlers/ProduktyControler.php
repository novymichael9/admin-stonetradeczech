<?php

class ProduktyControler extends Controler
{
    public function process($parameters)
    {
        $product = new Products();

        if (isset($_GET["smazat"]) && !empty($_GET["id"])) {
            $product->deleteProduct($_GET["id"]);
            $this->redirect("produkty");
        }

        $this->data['products']=$product->showTitleProduct();
        $this->header['title'] = 'Seznam produktů';
        $this->view = 'produkty';
    }
}