<?php

class VypisProduktControler extends Controler
{

    public function process($parameters)
    {
        $product = new Products();
        $category = new Categories();

        $prod = $product->showAllProduct($_GET['id']);
        $this->data = $prod;

        $this->data["product_category"] = $category->getCategoryNameById($prod["categories_id"]);
        $this->data["product_main_photo"] = $product->getMainPhoto($_GET["id"]);
        $this->data["product_photos"] = $product->getPhotos($_GET["id"]);
        if (isset($_GET['odstranit'])) {
            $delete = new Products();
            $delete->deleteProduct($_GET['odstranit']);
            $this->redirect('produkty');
        }
        $this->header['title'] = 'Vypis produktu - ID';
        $this->view = 'vypis-produkt';
    }
}