<?php

class PrihlaseniControler extends Controler
{
    public $model;

    public function process($parameters)
    {
        if ($_POST) {
            $user = new LoginUser();
            $user->login($_POST['username'], $_POST['password']);
            if (isset($_SESSION['user_id'])) {
                $this->redirect('menu');
            }
            $this->redirect('prihlaseni');
        }
        $this->header['title'] = 'Přihlášení do administrace';
        $this->view = ('prihlaseni');
    }
}