<?php

class MenuControler extends Controler
{
    public function process($parameters)
    {
        if(isset($_GET['odhlasit']))
        {
            session_destroy();
            $this->redirect('prihlaseni');
        }
        if(!isset($_SESSION['user_id']))
        {
            $this->redirect('prihlaseni');
        }
        $this->header['title'] = 'Menu administrace';
        $this->view = 'menu';
    }
}