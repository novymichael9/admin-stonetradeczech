<?php

class PocetNavstevControler extends Controler
{
    public function process($parameters)
    {
        $categories = new Categories();

        $this->data["categories"] = $categories->getAll();
        $this->data["categoriesModel"] = $categories;
        $this->header['title'] = 'Počet návštěv';
        $this->view = 'pocet-navstev';
    }
}