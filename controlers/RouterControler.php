<?php

class RouterControler extends Controler
{
    protected $controler;

    public function process($parametres)
    {
        $parsedURL = $this->parseURL($parametres[0]);

        if (empty($parsedURL[0]))
        {
            $this->redirect('prihlaseni');
        }
        $classControler = $this->dashesCamel(array_shift($parsedURL)) . 'Controler';

        if (file_exists('controlers/' . $classControler . '.php'))
        {
            $this->controler = new $classControler;
        }
        else
        {
            $this->redirect('error');
        }
        $this->controler->process($parsedURL);
        $this->data['title'] = $this->controler->header['title'];
        $this->view = 'layout';
    }

    private function parseURL($url)
    {
        $parsedURL = parse_url($url);
        $parsedURL["path"] = ltrim($parsedURL["path"], "/");
        $parsedURL["path"] = trim($parsedURL["path"]);
        $dividedWay = explode("/", $parsedURL["path"]);
        return $dividedWay;
    }

    private function dashesCamel($text)
    {
        $sentence = str_replace('-', ' ', $text);
        $sentence = ucwords($sentence);
        $sentence = str_replace(' ', '', $sentence);
        return $sentence;
    }
}