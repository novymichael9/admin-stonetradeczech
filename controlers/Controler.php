<?php

abstract class Controler
{
    protected $data = array();
    protected $view = "";
    protected $header = array('title' => '');

    abstract function process($parameters);

    public function extractView()
    {
        if ($this->view)
        {
            extract($this->data);
            require("views/" . $this->view . ".phtml");
        }
    }

    public function redirect($url)
    {
        header("Location: /$url");
        header("Connection: close");
        exit;
    }
}