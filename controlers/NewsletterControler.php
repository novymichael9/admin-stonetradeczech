<?php

class NewsletterControler extends Controler
{
    public function process($parameters)
    {
        $newsletter = new EmailNewsletter();
        $this->data['emails']=$newsletter->getEmails();
        $this->header['title'] = ['Emaily - Newsletter'];
        $this->view = 'newsletter';
    }
}