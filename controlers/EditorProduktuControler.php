<?php

class EditorProduktuControler extends Controler
{
    public function process($parameters)
    {
        $product = new Products();
        $categories = new Categories();

        if (!empty($_POST)) {
            if (!empty($_GET["id"])) {
                if (!empty($_FILES["images"])) {
                    foreach ($_FILES["images"]["name"] as $key => $name) {
                        $data_uri = $this->uploadFile("images", $key);
                        if (!empty($data_uri)) {
                            $product->addPhoto($_GET["id"], $data_uri);
                            if (empty($product->getMainPhoto($_GET["id"]))) {
                                $product->setMainPhoto($_GET["id"], Db::getLastInsertId());
                            }
                        }
                    }
                }
                if (!empty($_POST['action'])) {
                    $_POST['action-price-notax'] = 0;
                }
                $url = $this->friendly_url($_POST['name-product']);
                $product->editProduct($_POST['id-product'], $_POST['name-product'],$url,  $_POST['product-number'], $_POST['short-description-product'], $_POST['description-product'], $_POST['price-notax'],$_POST['action-price-notax'], $_POST['vat'], $_POST['category'], (!empty($_POST['in_stock']) ? 1 : 0), (!empty($_POST['sale']) ? 1 : 0));
            } else {
                if (!empty($_POST['action'])) {
                    $_POST['action-price-notax'] = 0;
                }
                if (!empty($_POST['action'])) {
                    $_POST['action-price-notax'] = 0;
                }
                $url = $this->friendly_url($_POST['name-product']);
                $isSaved = $product->addProduct($_POST['id-product'], $_POST['name-product'], $url, $_POST['product-number'], $_POST['short-description-product'], $_POST['description-product'], $_POST['price-notax'], $_POST['action-price-notax'], $_POST['vat'], $_POST['category'], (!empty($_POST['in_stock']) ? 1 : 0), (!empty($_POST['sale']) ? 1 : 0));
                if (!empty($_FILES["images"])) {
                    $product_id = Db::getLastInsertId();
                    foreach ($_FILES["images"]["name"] as $key => $name) {
                        $data_uri = $this->uploadFile("images", $key);
                        if (!empty($data_uri)) {
                            $product->addPhoto($product_id, $data_uri);
                            if (empty($product->getMainPhoto($product_id))) {
                                $product->setMainPhoto($product_id, Db::getLastInsertId());
                            }
                        }
                    }
                }
                if (!$isSaved) {
                    //$_SESSION['message'] = 'Už takový produkt máš v databázi.';
                } else {
                    //$_SESSION['message'] = 'Váš produkt byl uložen.';
                }
            }
            $this->redirect("produkty");
        }

        if (!empty($_GET["id"])) {
            if (!empty($_GET["smazatFotku"])) {
                $product->deletePhoto($_GET["smazatFotku"]);
                $this->redirect("editor-produktu?id=" . $_GET["id"]);
            }
            if (!empty($_GET["hlFoto"])) {
                $product->setMainPhoto($_GET["id"], $_GET["hlFoto"]);
                $this->redirect("editor-produktu?id=" . $_GET["id"]);
            }
            $this->data["product"] = $product->showAllProduct($_GET["id"]);
            $this->data["product_main_photo"] = $product->getMainPhoto($_GET["id"]);
            $this->data["product_photos"] = $product->getPhotos($_GET["id"]);
        }
        $this->header['title'] = 'Editor produktu';
        $this->data['categories'] = $categories->getAll();
        $this->view = ('editor-produktu');
    }

    private function uploadFile($files_key_name, $key = 0)
    {
        try {
            if (
                !isset($_FILES[$files_key_name]['error'][$key]) ||
                is_array($_FILES[$files_key_name]['error'][$key])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            switch ($_FILES[$files_key_name]['error'][$key]) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            if ($_FILES[$files_key_name]['size'][$key] > 1000000) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                    $finfo->file($_FILES[$files_key_name]['tmp_name'][$key]),
                    array(
                        'jpg' => 'image/jpeg',
                        'png' => 'image/png',
                        'gif' => 'image/gif',
                    ),
                    true
                )) {
                throw new RuntimeException('Invalid file format.');
            }

            $file_name = sha1_file($_FILES[$files_key_name]['tmp_name'][$key]) . "." . $ext;

            if (!move_uploaded_file(
                $_FILES[$files_key_name]['tmp_name'][$key],
                sprintf('./img/%s',
                    $file_name
                )
            )) {
                throw new RuntimeException('Failed to move uploaded file.');
            } else {
                $base64 = 'data:' . mime_content_type("./img/" . $file_name) . ';base64,' . base64_encode(file_get_contents("./img/" . $file_name));

                return $base64;
            }
        } catch (RuntimeException $e) {
            $e->getMessage();
        }
    }

    private function friendly_url($name) {
        $url = $name;
        $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
        $url = trim($url, "-");
        $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
        $url = strtolower($url);
        $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
        return $url;
    }

}